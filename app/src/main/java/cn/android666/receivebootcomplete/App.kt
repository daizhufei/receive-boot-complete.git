package cn.android666.receivebootcomplete

import android.app.Application
import android.util.Log

class App : Application() {

    companion object {
        lateinit var sInstance: App
    }

    override fun onCreate() {
        super.onCreate()
        sInstance = this
        Log.i("ABCD", "App onCreate: ${javaClass.simpleName} - ${hashCode()}")
    }
}