package cn.android666.receivebootcomplete

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.i("ABCD", "MainActivity onCreate")
    }

    override fun onPause() {
        super.onPause()
        Log.i("ABCD", "MainActivity onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i("ABCD", "MainActivity onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("ABCD", "MainActivity onDestroy")
    }
}